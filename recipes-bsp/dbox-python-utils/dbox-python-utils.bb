
DESCRIPTION = "Basic config utulities for D-Box"
DEPENDS = "python3"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}-${PV}:"

SRCREV = "${AUTOREV}"
# SRCREV = "601e9acb6f8ec89931ce040f8684380def341d96"
SRC_URI = "gitsm://bitbucket.org/d-box/dbox-python-utils.git;protocol=https"

S = "${WORKDIR}/git"

FILES_${PN} += "/usr/share/dbox-py/*"
FILES_${PN} += "/usr/share/dbox-py/config_watch/*"
FILES_${PN} += "/usr/share/dbox-py/asyncinotify/*"
FILES_${PN} += "/usr/share/dbox-py/asyncinotify/asyncinotify/*"
FILES_${PN} += "/usr/bin/dbox-config-change-watch"
FILES_${PN} += "/usr/bin/dbox-modem-init "

do_install() {

    mkdir -p ${D}/usr/share/dbox-py/config_watch
    mkdir -p ${D}/usr/share/dbox-py/asyncinotify/asyncinotify
    mkdir -p ${D}/usr/bin

    install -m 755 ${S}/*.py ${D}/usr/share/dbox-py/
    install -m 644 ${S}/config_watch/*.py ${D}/usr/share/dbox-py/config_watch
    install -m 644 ${S}/asyncinotify/asyncinotify/*.py ${D}/usr/share/dbox-py/asyncinotify/asyncinotify/
    
    lnr ${D}/usr/share/dbox-py/config_change_watch.py ${D}/usr/bin/dbox-config-change-watch
    lnr ${D}/usr/share/dbox-py/ensure_modem_usb_boot.py ${D}/usr/bin/dbox-modem-init 
}
