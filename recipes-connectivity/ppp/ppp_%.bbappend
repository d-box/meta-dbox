FILESEXTRAPATHS_prepend := "${THISDIR}:"

SRC_URI += "file://chat"
SRC_URI += "file://options"
SRC_URI += "file://options_debug"
 
do_install_append() {
    install -m 644 ${WORKDIR}/options ${D}${sysconfdir}/ppp
    install -m 644 ${WORKDIR}/options_debug ${D}${sysconfdir}/ppp
    install -m 644 ${WORKDIR}/chat ${D}${sysconfdir}/ppp
    echo -e '*\t*\t*\t*' >> ${D}${sysconfdir}/ppp/pap-secrets
    echo -e '*\t*\t*\t*' >> ${D}${sysconfdir}/ppp/chap-secrets
}
