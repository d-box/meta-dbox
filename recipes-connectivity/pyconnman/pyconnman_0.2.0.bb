SUMMARY = "A library for managing network connectivity using Python, ConnMan and DBus."
DESCRIPTION = "A library for managing network connectivity using Python, ConnMan and DBus."
HOMEPAGE = "https://github.com/liamw9534/pyconnman"
SECTION = "devel/python"

LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://LICENSE;md5=3b83ef96387f14655fc854ddc3c6bd57"
# 
SRC_URI[sha256sum] = "d3a63a039c82b08a1171b003eafa62c6f128aa4eaa1ce7a55a9401b48f9ad926"

PYPI_PACKAGE="pyconnman"

inherit pypi setuptools3

# RDEPENDS_${PN}_class-target += "\
#     ${PYTHON_PN}-aenum \
#     ${PYTHON_PN}-ctypes \
#     ${PYTHON_PN}-logging \
#     ${PYTHON_PN}-misc \
#     ${PYTHON_PN}-netserver \
#     ${PYTHON_PN}-sqlite3 \
#     ${PYTHON_PN}-wrapt \
# "
