#pragma once

#define IMX_GPIO_NR(bank, nr)		(((bank) - 1) * 32 + (nr))

/* GPS */
#define GPS_ON_PINNO			IMX_GPIO_NR(5,3)
#define GPS_NRESET_PINNO		IMX_GPIO_NR(5,4)

/* GSM */
#define GSM_NRESET_PINNO		IMX_GPIO_NR(4,11)

/* BT */
#define BT_NRESET_PINNO			IMX_GPIO_NR(1,2)
#define BT_DIRECTION_PINNO      IMX_GPIO_NR(5,5)

/* GPI */
#define GPI_1_PINNO				IMX_GPIO_NR(1,14)
#define GPI_2_PINNO				IMX_GPIO_NR(1,12)
#define GPI_3_PINNO				IMX_GPIO_NR(1,10)
#define GPI_4_PINNO				IMX_GPIO_NR(1,3)

/* GPO */
#define GPO_1_PINNO				IMX_GPIO_NR(1,1)
#define GPO_2_PINNO				IMX_GPIO_NR(1,5)
#define GPO_3_PINNO				IMX_GPIO_NR(1,8)
#define GPO_4_PINNO				IMX_GPIO_NR(1,6)

