#!/bin/bash

start() {
    echo "Loading DBOX-GPIO"
    /sbin/modprobe dbox_gpio
}

stop() {
    echo "Unloading DBOX-GPIO"
    /sbin/modprobe -r dbox_gpio
}

case "$1" in
  start)
	start
	;;
  stop)
	stop
	;;
  restart)
	stop
	start
	;;
  *)
	echo $"Usage: $prog {start|stop|restart}"
	exit 1
esac
