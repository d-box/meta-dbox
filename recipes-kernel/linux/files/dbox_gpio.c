#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <asm/io.h>
#include <linux/slab.h>
#include <linux/jiffies.h>
#include <linux/kobject.h>
#include <linux/sysfs.h>
#include <linux/pm.h>

#include <asm/siginfo.h>
#include <linux/rcupdate.h>
#include <linux/sched.h>
#include <linux/sched/signal.h>
#include <linux/delay.h>

#include <linux/hrtimer.h>
#include <linux/ktime.h>

#include <linux/irq.h>
#include <linux/interrupt.h>
#include <linux/gpio.h>

#include "dbox_pindefs.h"

#define MS_TO_NS(x)	(x * 1E6L)
#define LOCAL_DEBUG	pr_debug

// #define USE_EXTERNAL_BLUETOOTH

static struct gpio dbox_gpios[] = {
	/* Outputs */
	{ GPS_ON_PINNO,				GPIOF_OUT_INIT_LOW,		"GPS_ON" },
	{ GSM_NRESET_PINNO, 		GPIOF_OUT_INIT_HIGH,	"GSM_RESET" },
#ifdef USE_EXTERNAL_BLUETOOTH
	{ BT_NRESET_PINNO,			GPIOF_OUT_INIT_HIGH,	"BT_NRESET" },
	{ BT_DIRECTION_PINNO, 		GPIOF_OUT_INIT_HIGH,	"BT_DIRECTION" },
#else
    { BT_NRESET_PINNO,			GPIOF_OUT_INIT_LOW,	"BT_NRESET" },
#endif
	{ GPO_1_PINNO,				GPIOF_OUT_INIT_LOW,		"GPO_1" },
	{ GPO_2_PINNO,				GPIOF_OUT_INIT_LOW,		"GPO_2" },
	{ GPO_3_PINNO,				GPIOF_OUT_INIT_LOW,		"GPO_3" },
	{ GPO_4_PINNO,				GPIOF_OUT_INIT_LOW,		"GPO_4" },
	
	/* Inputs */
	{ GPI_1_PINNO,				GPIOF_IN, 				"GPI_1" },
	{ GPI_2_PINNO,				GPIOF_IN, 				"GPI_2" },
	{ GPI_3_PINNO,				GPIOF_IN, 				"GPI_3" },
	{ GPI_4_PINNO,				GPIOF_IN, 				"GPI_4" },
};

static int app_pid = 0;
module_param(app_pid, int, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
MODULE_PARM_DESC(app_pid, "Pid of application to send signals to");

static int dev_id=16359;

static struct _dbox_gpio_state {
	uint gps_on;
} gpio_state;

static struct _dbox_irq_defs {
	unsigned int pin;
	unsigned long flags;
	const char *device;
	bool wake;
	bool disabled;
	uint irqno;
	uint signo;
	int prev_state;
	bool push_ignores_prev_state;
} dbox_irq_defs[] = {
	{
		.pin = GPI_1_PINNO,
		.flags = IRQF_TRIGGER_RISING,
		.device = "GPI_1",
		.wake = false,
		.disabled = false,
		.irqno = 0,
		.signo = 0,
		.push_ignores_prev_state = true
	},
	{
		.pin = GPI_2_PINNO,
		.flags = IRQF_TRIGGER_RISING,
		.device = "GPI_2",
		.wake = false,
		.disabled = false,
		.irqno = 0,
		.signo = 0,
		.push_ignores_prev_state = true
	},
	{
		.pin = GPI_3_PINNO,
		.flags = IRQF_TRIGGER_RISING,
		.device = "GPI_3",
		.wake = false,
		.disabled = false,
		.irqno = 0,
		.signo = 0,
		.push_ignores_prev_state = true
	},
	{
		.pin = GPI_4_PINNO,
		.flags = IRQF_TRIGGER_RISING,
		.device = "GPI_4",
		.wake = false,
		.disabled = false,
		.irqno = 0,
		.signo = 0,
		.push_ignores_prev_state = true
	},
};

bool dbox_gpio_send_signal(int sig, int pid);

/**
* Handler IRQs
*/
static irqreturn_t gpio_interrupt_handler(int irq, void* dev_id) {
	int i, state;
	bool updated = false;
	// pr_info("GPIO IRQ fired: ");
	if (app_pid) {
		for (i=0; i < ARRAY_SIZE(dbox_irq_defs); ++i) {
			if (irq != dbox_irq_defs[i].irqno) {
				continue;
			}

			// pr_info("%s - signo %d disabled %d\n", dbox_irq_defs[i].device, dbox_irq_defs[i].signo, dbox_irq_defs[i].disabled);

			if(
				dbox_irq_defs[i].signo == 0
				|| dbox_irq_defs[i].disabled
			) {
				continue;
			}

			state = gpio_get_value(dbox_irq_defs[i].pin);
			if(!dbox_irq_defs[i].push_ignores_prev_state &&
					dbox_irq_defs[i].prev_state == state) {
				continue;
			}

			dbox_irq_defs[i].prev_state = state;

			if(state != 0) {
				if ((dbox_irq_defs[i].flags & (IRQF_TRIGGER_RISING || IRQF_TRIGGER_HIGH)) == 0) {
					continue;
				}
			} else {
				if ((dbox_irq_defs[i].flags & (IRQF_TRIGGER_FALLING || IRQF_TRIGGER_LOW)) == 0) {
					continue;
				}
			}

			dbox_gpio_send_signal(dbox_irq_defs[i].signo, app_pid);
			updated = true;
			break;
		}
		// if(!updated)
		// 	pr_info("Unknown - %d\n", irq);
	} else {
		// pr_info("Disabled\n");
	}
	return IRQ_HANDLED;
}


static bool set_irq_state(uint pinno, const char *buf, size_t count)
{
	int i;
	uint temp;
	bool disabled;

	if (sscanf(buf, "%du", &temp) <= 0) {
		return false;
	}

	disabled = temp == 0;

	for (i=0; i < ARRAY_SIZE(dbox_irq_defs); ++i) {
		if (dbox_irq_defs[i].pin == pinno) {
			if(dbox_irq_defs[i].disabled != disabled) {
				// can't actually disable the IRQ, each port only has 2 IRQs
				// disable_irq(dbox_irq_defs[i].irqno);
				dbox_irq_defs[i].disabled = disabled;
			}
			return true;
		}
	}

	return false;
}

static bool set_irq_signo(uint pinno, const char *buf, size_t count)
{
	int i;
	uint temp;

	if (sscanf(buf, "%du", &temp) <= 0) {
		return false;
	}

	for (i=0; i < ARRAY_SIZE(dbox_irq_defs); ++i) {
		if (dbox_irq_defs[i].pin == pinno) {
			dbox_irq_defs[i].signo = temp;
			return true;
		}
	}

	return false;
}

static uint get_irq_signo(uint pinno)
{
	int i;

	for (i=0; i < ARRAY_SIZE(dbox_irq_defs); ++i) {
		if (dbox_irq_defs[i].pin == pinno) {
			return dbox_irq_defs[i].signo;
		}
	}

	return 0;
}

/* ------------------- APP PID ------------------- */
// For getting set app process PID number
static ssize_t app_pid_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
	return sprintf(buf, "%d\n", app_pid);
}

// For setting app process PID number
static ssize_t app_pid_store(struct kobject *kobj, struct kobj_attribute *attr, const char *buf, size_t count)
{
	uint temp;
	if (sscanf(buf, "%du", &temp) > 0)
		app_pid = temp;
	return count;
}
static struct kobj_attribute app_pid_attribute		= __ATTR(app_pid, 0664, app_pid_show, app_pid_store);

/* ------------------- GPS State ------------------- */
static ssize_t dbox_gps_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
	return sprintf(buf, "%d\n", gpio_state.gps_on);
}

static ssize_t dbox_gps_store(struct kobject *kobj, struct kobj_attribute *attr, const char *buf, size_t count)
{
	uint temp;
	if (sscanf(buf, "%du", &temp) > 0 && temp != gpio_state.gps_on ) {
		gpio_state.gps_on = temp;
		if (gpio_state.gps_on) {
			gpio_direction_output(GPS_ON_PINNO, 1);;
		} else {
			gpio_direction_output(GPS_ON_PINNO, 0);
		}
	}

	return count;
}
static struct kobj_attribute dbox_gps_state		= __ATTR(gps_state, 0664, dbox_gps_show, dbox_gps_store);
/* --------------------------------------------- */

/* ------------------- GSM nReset ------------------- */
static ssize_t gsm_reset_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
	return sprintf(buf, "%d\n", gpio_get_value(GSM_NRESET_PINNO) ? 0 : 1);
}

static ssize_t gsm_reset_store(struct kobject *kobj, struct kobj_attribute *attr, const char *buf, size_t count)
{
	uint temp;
	if (sscanf(buf, "%du", &temp) > 0) {
		if (temp) {
			gpio_direction_output(GSM_NRESET_PINNO, 0);;
		} else {
			gpio_direction_output(GSM_NRESET_PINNO, 1);
		}
	}

	return count;
}
static struct kobj_attribute dbox_gsm_reset		= __ATTR(gsm_reset, 0664, gsm_reset_show, gsm_reset_store);
/* --------------------------------------------- */

/* ------------------- BT nReset ------------------- */
static ssize_t bt_reset_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
	return sprintf(buf, "%d\n", gpio_get_value(BT_NRESET_PINNO) ? 0 : 1);
}

static ssize_t bt_reset_store(struct kobject *kobj, struct kobj_attribute *attr, const char *buf, size_t count)
{
	uint temp;
	if (sscanf(buf, "%du", &temp) > 0) {
		if (temp) {
			gpio_direction_output(BT_NRESET_PINNO, 0);;
		} else {
			gpio_direction_output(BT_NRESET_PINNO, 1);
		}
	}

	return count;
}
static struct kobj_attribute dbox_bt_reset		= __ATTR(bt_reset, 0664, bt_reset_show, bt_reset_store);
/* --------------------------------------------- */
#ifdef USE_EXTERNAL_BLUETOOTH
/* ------------------- BT Direction ------------------- */
static ssize_t bt_direction_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
	return sprintf(buf, "%d\n", gpio_get_value(BT_DIRECTION_PINNO));
}

static ssize_t bt_direction_store(struct kobject *kobj, struct kobj_attribute *attr, const char *buf, size_t count)
{
	uint temp;
	if (sscanf(buf, "%du", &temp) > 0) {
		if (temp) {
			gpio_direction_output(BT_DIRECTION_PINNO, 1);;
		} else {
			gpio_direction_output(BT_DIRECTION_PINNO, 0);
		}
	}

	return count;
}
static struct kobj_attribute dbox_bt_direction		= __ATTR(bt_direction, 0664, bt_direction_show, bt_direction_store);
/* --------------------------------------------- */
#endif
/* ------------------- GPO_1 ------------------- */
static ssize_t dbox_gpo1_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
	return sprintf(buf, "%d\n", gpio_get_value(GPO_1_PINNO));
}

static ssize_t dbox_gpo1_store(struct kobject *kobj, struct kobj_attribute *attr, const char *buf, size_t count)
{
	uint temp;
	if (sscanf(buf, "%du", &temp) > 0) {
		if (temp) {
			gpio_direction_output(GPO_1_PINNO, 1);;
		} else {
			gpio_direction_output(GPO_1_PINNO, 0);
		}
	}

	return count;
}
static struct kobj_attribute dbox_gpo1_state		= __ATTR(gpo1, 0664, dbox_gpo1_show, dbox_gpo1_store);
/* --------------------------------------------- */

/* ------------------- GPO_2 ------------------- */
static ssize_t dbox_gpo2_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
	return sprintf(buf, "%d\n", gpio_get_value(GPO_2_PINNO));
}

static ssize_t dbox_gpo2_store(struct kobject *kobj, struct kobj_attribute *attr, const char *buf, size_t count)
{
	uint temp;
	if (sscanf(buf, "%du", &temp) > 0) {
		if (temp) {
			gpio_direction_output(GPO_2_PINNO, 1);;
		} else {
			gpio_direction_output(GPO_2_PINNO, 0);
		}
	}

	return count;
}
static struct kobj_attribute dbox_gpo2_state		= __ATTR(gpo2, 0664, dbox_gpo2_show, dbox_gpo2_store);
/* --------------------------------------------- */

/* ------------------- GPO_3 ------------------- */
static ssize_t dbox_gpo3_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
	return sprintf(buf, "%d\n", gpio_get_value(GPO_3_PINNO));
}

static ssize_t dbox_gpo3_store(struct kobject *kobj, struct kobj_attribute *attr, const char *buf, size_t count)
{
	uint temp;
	if (sscanf(buf, "%du", &temp) > 0) {
		if (temp) {
			gpio_direction_output(GPO_3_PINNO, 1);;
		} else {
			gpio_direction_output(GPO_3_PINNO, 0);
		}
	}

	return count;
}
static struct kobj_attribute dbox_gpo3_state		= __ATTR(gpo3, 0664, dbox_gpo3_show, dbox_gpo3_store);
/* --------------------------------------------- */

/* ------------------- GPO_4 ------------------- */
static ssize_t dbox_gpo4_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
	return sprintf(buf, "%d\n", gpio_get_value(GPO_4_PINNO));
}

static ssize_t dbox_gpo4_store(struct kobject *kobj, struct kobj_attribute *attr, const char *buf, size_t count)
{
	uint temp;
	if (sscanf(buf, "%du", &temp) > 0) {
		if (temp) {
			gpio_direction_output(GPO_4_PINNO, 1);;
		} else {
			gpio_direction_output(GPO_4_PINNO, 0);
		}
	}

	return count;
}
static struct kobj_attribute dbox_gpo4_state		= __ATTR(gpo4, 0664, dbox_gpo4_show, dbox_gpo4_store);
/* --------------------------------------------- */

/* ------------------- GPI_1 ------------------- */
static ssize_t dbox_gpi1_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
	return sprintf(buf, "%d\n", gpio_get_value(GPI_1_PINNO));
}

static ssize_t dbox_gpi1_set_state(struct kobject *kobj, struct kobj_attribute *attr, const char *buf, size_t count)
{
	set_irq_state(GPI_1_PINNO, buf, count);
	return count;
}

static struct kobj_attribute dbox_gpi1_state		= __ATTR(gpi1, 0664, dbox_gpi1_show, dbox_gpi1_set_state);

static ssize_t dbox_gpi1_signo_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
	return sprintf(buf, "%d\n", get_irq_signo(GPI_1_PINNO));
}

static ssize_t dbox_gpi1_signo_store(struct kobject *kobj, struct kobj_attribute *attr, const char *buf, size_t count)
{
	set_irq_signo(GPI_1_PINNO, buf, count);
	return count;
}

static struct kobj_attribute dbox_gpi1_signo		= __ATTR(gpi1_signo, 0664, dbox_gpi1_signo_show, dbox_gpi1_signo_store);
/* --------------------------------------------- */

/* ------------------- GPI_2 ------------------- */
static ssize_t dbox_gpi2_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
	return sprintf(buf, "%d\n", gpio_get_value(GPI_2_PINNO));
}

static ssize_t dbox_gpi2_set_state(struct kobject *kobj, struct kobj_attribute *attr, const char *buf, size_t count)
{
	set_irq_state(GPI_2_PINNO, buf, count);
	return count;
}

static struct kobj_attribute dbox_gpi2_state		= __ATTR(gpi2, 0664, dbox_gpi2_show, dbox_gpi2_set_state);

static ssize_t dbox_gpi2_signo_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
	return sprintf(buf, "%d\n", get_irq_signo(GPI_2_PINNO));
}

static ssize_t dbox_gpi2_signo_store(struct kobject *kobj, struct kobj_attribute *attr, const char *buf, size_t count)
{
	set_irq_signo(GPI_2_PINNO, buf, count);
	return count;
}

static struct kobj_attribute dbox_gpi2_signo		= __ATTR(gpi2_signo, 0664, dbox_gpi2_signo_show, dbox_gpi2_signo_store);
/* --------------------------------------------- */

/* ------------------- GPI_3 ------------------- */
static ssize_t dbox_gpi3_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
	return sprintf(buf, "%d\n", gpio_get_value(GPI_3_PINNO));
}

static ssize_t dbox_gpi3_set_state(struct kobject *kobj, struct kobj_attribute *attr, const char *buf, size_t count)
{
	set_irq_state(GPI_3_PINNO, buf, count);
	return count;
}

static struct kobj_attribute dbox_gpi3_state		= __ATTR(gpi3, 0664, dbox_gpi3_show, dbox_gpi3_set_state);

static ssize_t dbox_gpi3_signo_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
	return sprintf(buf, "%d\n", get_irq_signo(GPI_3_PINNO));
}

static ssize_t dbox_gpi3_signo_store(struct kobject *kobj, struct kobj_attribute *attr, const char *buf, size_t count)
{
	set_irq_signo(GPI_3_PINNO, buf, count);
	return count;
}

static struct kobj_attribute dbox_gpi3_signo		= __ATTR(gpi3_signo, 0664, dbox_gpi3_signo_show, dbox_gpi3_signo_store);
/* --------------------------------------------- */

/* ------------------- GPI_4 ------------------- */
static ssize_t dbox_gpi4_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
	return sprintf(buf, "%d\n", gpio_get_value(GPI_4_PINNO));
}

static ssize_t dbox_gpi4_set_state(struct kobject *kobj, struct kobj_attribute *attr, const char *buf, size_t count)
{
	set_irq_state(GPI_4_PINNO, buf, count);
	return count;
}

static struct kobj_attribute dbox_gpi4_state		= __ATTR(gpi4, 0664, dbox_gpi4_show, dbox_gpi4_set_state);

static ssize_t dbox_gpi4_signo_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
	return sprintf(buf, "%d\n", get_irq_signo(GPI_4_PINNO));
}

static ssize_t dbox_gpi4_signo_store(struct kobject *kobj, struct kobj_attribute *attr, const char *buf, size_t count)
{
	set_irq_signo(GPI_4_PINNO, buf, count);
	return count;
}

static struct kobj_attribute dbox_gpi4_signo		= __ATTR(gpi4_signo, 0664, dbox_gpi4_signo_show, dbox_gpi4_signo_store);
/* --------------------------------------------- */

static struct kobject *dbox_kobj;

/*
* Create a group of attributes so that we can create and destroy them all
* at once.
*/
static struct attribute *attrs[] = {
	&app_pid_attribute.attr,
	&dbox_gps_state.attr,
	&dbox_gsm_reset.attr,
	&dbox_bt_reset.attr,
#ifdef USE_EXTERNAL_BLUETOOTH
	&dbox_bt_direction.attr,
#endif
	/* GPI */
	&dbox_gpi1_state.attr,
	&dbox_gpi1_signo.attr,
	&dbox_gpi2_state.attr,
	&dbox_gpi2_signo.attr,
	&dbox_gpi3_state.attr,
	&dbox_gpi3_signo.attr,
	&dbox_gpi4_state.attr,
	&dbox_gpi4_signo.attr,
	/* GPO */
	&dbox_gpo1_state.attr,
	&dbox_gpo2_state.attr,
	&dbox_gpo3_state.attr,
	&dbox_gpo4_state.attr,
	NULL,	/* need to NULL terminate the list of attributes */
};

static struct attribute_group attr_group = {
	.attrs = attrs,
};

static int __init dbox_init(void)
{
	int retval=0,i,irqno;

	retval = gpio_request_array(dbox_gpios, ARRAY_SIZE(dbox_gpios));
	if (retval) {
		gpio_free_array(dbox_gpios, ARRAY_SIZE(dbox_gpios));
		pr_err("dbox_gpio: Error Setting GPIOs: %d\n", retval);
		return retval;
	}

	dbox_kobj = kobject_create_and_add("dbox_gpio", kernel_kobj);
	if (!dbox_kobj) {
		pr_err("dbox_gpio: Could not init kobject\n");
		gpio_free_array(dbox_gpios, ARRAY_SIZE(dbox_gpios));
		return -ENOMEM;
	}

	retval = sysfs_create_group(dbox_kobj, &attr_group);
	if (retval) {
		pr_err("dbox_gpio: Could not create SYSFS file\n");
		gpio_free_array(dbox_gpios, ARRAY_SIZE(dbox_gpios));
		kobject_put(dbox_kobj);
		return retval;
	}

	for (i=0; i < ARRAY_SIZE(dbox_irq_defs); ++i) {
		irqno= gpio_to_irq(dbox_irq_defs[i].pin);
		if (irqno < 0) {
			pr_err("dbox_gpio: Error geting IRQ# for pin %d: %d\n", dbox_irq_defs[i].pin, irqno);
		}
		retval = request_irq(irqno, gpio_interrupt_handler, dbox_irq_defs[i].flags, dbox_irq_defs[i].device, (void*)&dev_id);
		if (retval) {
			pr_err("dbox_gpio: Error requesting IRQ %d for pin %d: %d\n", irqno, dbox_irq_defs[i].pin, retval);
		} else {
			if (dbox_irq_defs[i].wake) {
				pr_info("dbox_gpio: IRQ %d for pin %d enabling wake\n", irqno, dbox_irq_defs[i].pin);
				enable_irq_wake(irqno);
			}
			if (dbox_irq_defs[i].disabled) {
				pr_info("dbox_gpio: IRQ %d for pin %d disabling by default\n", irqno, dbox_irq_defs[i].pin);
				// can't actually disable the IRQ, each port only has 2 IRQs
				// disable_irq(dbox_irq_defs[i].irqno);
			}
			dbox_irq_defs[i].irqno = irqno;
			dbox_irq_defs[i].prev_state = gpio_get_value(dbox_irq_defs[i].pin);
			pr_info("dbox_gpio: Requested IRQ %d for pin %d\n", irqno, dbox_irq_defs[i].pin);
		}
	}
	return 0;    // Non-zero return means that the module couldn't be loaded.
}

static void __exit dbox_cleanup(void)
{
	int i;
	gpio_free_array(dbox_gpios, ARRAY_SIZE(dbox_gpios));
	kobject_put(dbox_kobj);

	gpio_direction_output(GPS_ON_PINNO, 0);
	for (i=0; i < ARRAY_SIZE(dbox_irq_defs); ++i) {
		free_irq(gpio_to_irq(dbox_irq_defs[i].pin), (void*)&dev_id);
	}
	pr_info("dbox_gpio: Module unload\n");
}

bool dbox_gpio_send_signal(int sig, int pid) {
	struct kernel_siginfo info;
	struct task_struct *t;

	if (pid && sig) {
		memset(&info, 0, sizeof(struct kernel_siginfo));
		info.si_signo = sig;
		info.si_code = SI_QUEUE;
		info.si_int = 1234;
		rcu_read_lock();
		t = pid_task(find_pid_ns(pid, &init_pid_ns), PIDTYPE_PID);
		if (t == NULL) {
			rcu_read_unlock();
			return false;
		}
		rcu_read_unlock();
		if (send_sig_info(sig, &info, t)) {    //send the signal
			pr_err("dbox_gpio: Error sending signal %d to PID %d\n", sig, pid);
			return false;
		}
		return true;
	}

	return false;
}

module_init(dbox_init);
module_exit(dbox_cleanup);

#undef LOCAL_DEBUG

MODULE_LICENSE("GPL");  // otherwise kernel complains

MODULE_AUTHOR("Juliusz Hoffman");
MODULE_DESCRIPTION("D-box GPIO management module");
