FILESEXTRAPATHS_prepend := "${THISDIR}:${THISDIR}/files:"

#defconfig
SRC_URI += "file://defconfig"
KBUILD_DEFCONFIG_imx8mm-var-dart = ""


#DTS
CUSTOM_DEVICETREE = "imx8mm-var-dart-dbox"
DEFAULT_DTB_PREFIX_imx8mq-var-dart = "imx8mq-var-dart-dbox"

SRC_URI += "file://${CUSTOM_DEVICETREE}.dts"

# custom device drivers
SRC_URI += "file://custom_device_drivers_base.patch"
SRC_URI += "file://dbox_gpio.c"
SRC_URI += "file://dbox_pindefs.h"
 
do_configure_append() {
    # DTS
	cp ${WORKDIR}/${CUSTOM_DEVICETREE}.dts ${S}/arch/arm64/boot/dts/freescale
	
	# custom device drivers
	mkdir -p ${S}/drivers/custom
	cp ${WORKDIR}/dbox_gpio.c ${S}/drivers/custom
	cp ${WORKDIR}/dbox_pindefs.h ${S}/drivers/custom
	
	#kernel_conf_variable CUSTOM y
	#kernel_conf_variable DBOX_GPIO m
}
