FILESEXTRAPATHS_prepend := "${THISDIR}:"

SRC_URI += "file://ppp_setup.rules"
 
do_install_append() {
    install -D -m 644 ${WORKDIR}/ppp_setup.rules ${D}${sysconfdir}/udev/rules.d
}
