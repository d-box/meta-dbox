DESCRIPTION = "A console-only image with docker-ce support"
LICENSE = "MIT"

inherit core-image features_check

REQUIRED_DISTRO_FEATURES += "virtualization"

IMAGE_FEATURES += "ssh-server-dropbear"

IMAGE_INSTALL += " \
    docker-ce \
    python3-docker-compose \
    packagegroup-basic \
    packagegroup-core-full-cmdline \
    curl \
    dosfstools \
    fio \
    i2c-tools \
    imx-kobs \
    ldd \
    memtester \
    mtd-utils \
    mtd-utils-ubifs \
    screen \
    wireless-regdb-static \
    pm-utils \
    wpa-supplicant \
    imx-kobs \
	bcm43xx-utils \
	brcm-patchram-plus \
	linux-firmware-bcm4339 \
	linux-firmware-bcm43430 \
	rsync \
	dbox-python-utils \
"

MACHINE_EXTRA_RDEPENDS += " \
    pm-utils \
    wpa-supplicant \
    imx-kobs \
	bcm43xx-utils \
	brcm-patchram-plus \
	linux-firmware-bcm4339 \
	linux-firmware-bcm43430 \
"
